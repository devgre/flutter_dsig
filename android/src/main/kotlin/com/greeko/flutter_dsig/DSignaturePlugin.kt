package com.greeko.flutter_dsig

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import java.io.ByteArrayInputStream
import java.io.EOFException
import java.io.IOException
import java.security.KeyStore
import java.security.PrivateKey
import java.security.Signature
import java.security.cert.X509Certificate

/** DSignaturePlugin */
class DSignaturePlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_dsig")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
    when (call.method) {
      "getPlatformVersion" -> platformVersion(result)
      "getCertificateFromP12" -> getCertificateFromP12(call.argument<ByteArray>("p12")!!,
              call.argument<String>("password")!!,
              result)
      "getPrivateKeyFromP12" -> getPrivateKeyFromP12(call.argument<ByteArray>("p12")!!,
              call.argument<String>("password")!!,
              result)
      "getNodesFromP12" -> getNodesFromP12(call.argument<ByteArray>("p12")!!,
              call.argument<String>("password")!!,
              result)
      "signDataWithP12" -> signWithP12(
              call.argument<ByteArray>("data")!!,
              call.argument<ByteArray>("p12")!!,
              call.argument<String>("password")!!,
              result)
      else -> result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  private fun platformVersion(result: MethodChannel.Result) {
    result.success("Android v${android.os.Build.VERSION.RELEASE}")
  }

  private fun getCertificateFromP12(p12: ByteArray, password: String, result: MethodChannel.Result) {
    try {
      val certificate = getCertificate(p12, password)
      result.success(certificate)
    } catch (ex: IOException) {
      result.error("BAD_PASSWORD", ex.message, null)
    } catch (ex: EOFException) {
      result.error("BAD_CERTIFICATE_FORMAT", ex.message, null)
    } catch (ex: java.lang.Exception) {
      result.error("CERTIFICATE_ERROR", ex.message, null)
    }
  }

  private fun getPrivateKeyFromP12(p12: ByteArray, password: String, result: MethodChannel.Result) {
    try {
      val pk = getPrivateKey(p12, password)
      result.success(pk.privateKey.encoded!!)
    } catch (ex: IOException) {
      result.error("BAD_PASSWORD", ex.message, null)
    } catch (ex: EOFException) {
      result.error("BAD_CERTIFICATE_FORMAT", ex.message, null)
    } catch (ex: java.lang.Exception) {
      result.error("CERTIFICATE_ERROR", ex.message, null)
    }
  }

  private fun getNodesFromP12(p12: ByteArray, password: String, result: MethodChannel.Result) {
    try {
      val nodes = getNodes(p12, password)
      result.success(nodes)
    } catch (ex: IOException) {
      result.error("BAD_PASSWORD", ex.message, null)
    } catch (ex: EOFException) {
      result.error("BAD_CERTIFICATE_FORMAT", ex.message, null)
    } catch (ex: java.lang.Exception) {
      result.error("CERTIFICATE_ERROR", ex.message, null)
    }
  }

  private fun signWithP12(data: ByteArray, p12: ByteArray, password: String, result: MethodChannel.Result) {
    try {
      val pk = getPrivateKey(p12, password)
      result.success(signWithPrivateKey(data, pk.privateKey))
    } catch (ex: IOException) {
      result.error("BAD_PASSWORD", ex.message, null)
    } catch (ex: EOFException) {
      result.error("BAD_CERTIFICATE_FORMAT", ex.message, null)
    } catch (ex: java.lang.Exception) {
      result.error("CERTIFICATE_ERROR", ex.message, null)
    }
  }

  private fun signWithPrivateKey(data: ByteArray, pk: PrivateKey): ByteArray {
    val sig = Signature.getInstance("SHA1WithRSA")
    sig.initSign(pk)
    sig.update(data)
    return sig.sign()
  }

  private fun getCertificate(data: ByteArray, password: String): ByteArray {
    val p12 = getP12(data, password)!!
    val e = p12.aliases()
    val alias = e.nextElement() as String
    val c = p12.getCertificate(alias) as X509Certificate
    return c.encoded!!
  }

  private fun getPrivateKey(data: ByteArray, password: String): KeyStore.PrivateKeyEntry {
    val p12 = getP12(data, password)!!
    val e = p12.aliases()
    val alias = e.nextElement() as String
    val entry = p12.getEntry(alias, KeyStore.PasswordProtection("".toCharArray()))
    return entry as KeyStore.PrivateKeyEntry
  }

  private fun getNodes(data: ByteArray, password: String): List<ByteArray> {
    val p12 = getP12(data, password)!!
    val e = p12.aliases()
    val alias = e.nextElement() as String

    val cert = p12.getCertificate(alias) as X509Certificate
    var pke = p12.getEntry(alias, KeyStore.PasswordProtection("".toCharArray())) as KeyStore.PrivateKeyEntry

    return listOf(
        cert.encoded!!,
        pke.privateKey.encoded!!
    )
  }

  private fun getP12(data: ByteArray, password: String): KeyStore? {
    val p12 = KeyStore.getInstance("pkcs12")
    p12.load(ByteArrayInputStream(data), password.toCharArray())
    return p12
  }
}
