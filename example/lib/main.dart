import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_dsig/flutter_dsig.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = await DSignature.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('DSignature example'),
        ),
        body: Column(children: [
          const SizedBox(height: 10.0),
          Text('Running on: $_platformVersion\n'),
          const SizedBox(height: 10.0),
          ElevatedButton(
            child: const Text('Test'),
            onPressed: () async {
              // var xml = '''
              //   <?xml version="1.0" encoding="UTF-8"?>
              //   <root>
              //     <id>f598bb0d-1653-4ef1-aa81-95fbcbab19fe</id>
              //     <data>![CDATA[<!DOCTYPE html><html><body></body></html>]]</data>
              //   </root>
              // ''';
              const xml = '''<?xml version="1.0" encoding="UTF-8"?><root><document><![CDATA[<!DOCTYPE html><html><body>Hello!!!</body></html>]]></document></root>''';

              const passwd = 'Qwerty12';
              final p12BytesData = await rootBundle.load('assets/RSA256_e3fe35adda3b45cbea3a3f1ed48f263dc55c556e.p12');
              final p12Bytes = p12BytesData.buffer.asUint8List();
              print('>>> p12Bytes count: ${p12Bytes.length}');

              var certificate = await DSignature.getCertificateFromP12(p12Bytes, passwd);
              log('>>> certificate: $certificate');

              var privateKey = await DSignature.getPrivateKeyFromP12(p12Bytes, passwd);
              log('>>> privateKey: $privateKey');

              // var nodes = await DSignature.getNodesFromP12(p12Bytes, passwd);
              // log('>>> nodes: $nodes');

              final xmlBytes = Uint8List.fromList(utf8.encode(xml));

              var signedXml = await DSignature.signWithP12(xmlBytes, p12Bytes, passwd);
              print('>>> signedXml: $signedXml');

            }
          )
        ]),
      ),
    );
  }
}
