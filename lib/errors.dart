
class NoCertificateException implements Exception {
  NoCertificateException(this.message);

  String? message;
}

class BadPasswordP12Exception implements Exception {
  BadPasswordP12Exception(this.message);

  String? message;
}

class BadFormatP12Exception implements Exception {
  BadFormatP12Exception(this.message);

  String? message;
}

class UnknownP12Exception implements Exception {
  UnknownP12Exception(this.message);

  String? message;
}