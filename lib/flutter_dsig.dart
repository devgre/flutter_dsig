
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/services.dart';

import 'errors.dart';

class DSignature {
  static const MethodChannel _channel = MethodChannel('flutter_dsig');

  static Future<String?> get platformVersion async =>
    _channel.invokeMethod('getPlatformVersion');

  static Future<String> getCertificateFromP12(Uint8List p12Bytes, String password) async {
    try {
      final Uint8List crtBytes = await _channel.invokeMethod("getCertificateFromP12", {
        'p12': p12Bytes,
        'password': password
      });

      return base64Encode(crtBytes);
    } catch (e) {
      if (e is PlatformException) {
        switch (e.code) {
          case "BAD_PASSWORD":            throw BadPasswordP12Exception(e.message);
          case "BAD_CERTIFICATE_FORMAT":  throw BadFormatP12Exception(e.message);
          case "CERTIFICATE_ERROR":       throw UnknownP12Exception(e.message);
          default: rethrow;
        }
      } else {
        rethrow;
      }
    }
  }

  static Future<String> getPrivateKeyFromP12(Uint8List p12Bytes, String password) async {
    try {
      final Uint8List crtBytes = await _channel.invokeMethod("getPrivateKeyFromP12", {
        'p12': p12Bytes,
        'password': password
      });

      return base64Encode(crtBytes);
    } catch (e) {
      if (e is PlatformException) {
        switch (e.code) {
          case "BAD_PASSWORD":            throw BadPasswordP12Exception(e.message);
          case "BAD_CERTIFICATE_FORMAT":  throw BadFormatP12Exception(e.message);
          case "CERTIFICATE_ERROR":       throw UnknownP12Exception(e.message);
          default: rethrow;
        }
      } else {
        rethrow;
      }
    }
  }

  static Future<List<String>> getNodesFromP12(Uint8List p12Bytes, String password) async {
    try {
      final List<Object?> nodes = await _channel.invokeMethod("getNodesFromP12", {
        'p12': p12Bytes,
        'password': password
      });

      return [
        base64Encode(nodes[0] as Uint8List),
        base64Encode(nodes[1] as Uint8List)
      ];
    } catch (e) {
      if (e is PlatformException) {
        switch (e.code) {
          case "BAD_PASSWORD":            throw BadPasswordP12Exception(e.message);
          case "BAD_CERTIFICATE_FORMAT":  throw BadFormatP12Exception(e.message);
          case "CERTIFICATE_ERROR":       throw UnknownP12Exception(e.message);
          default: rethrow;
        }
      } else {
        rethrow;
      }
    }
  }

  static Future<String> signWithP12(Uint8List dataBytes, Uint8List p12Bytes, String password) async {
    try {
      final Uint8List signatureBytes = await _channel.invokeMethod("signDataWithP12", {
        'data': dataBytes,
        'p12': p12Bytes,
        'password': password
      });

      return base64Encode(signatureBytes);
    } catch (e) {
      if (e is PlatformException) {
        switch (e.code) {
          case "BAD_PASSWORD":            throw BadPasswordP12Exception(e.message);
          case "BAD_CERTIFICATE_FORMAT":  throw BadFormatP12Exception(e.message);
          case "CERTIFICATE_ERROR":       throw UnknownP12Exception(e.message);
          default: rethrow;
        }
      } else {
        rethrow;
      }
    }
  }
}
