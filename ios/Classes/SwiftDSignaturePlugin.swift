import Flutter
import UIKit


let ErrBadPassword = FlutterError(code: "BAD_PASSWORD", message: "password is incorrect", details: nil)
let ErrCertificateExtraction = FlutterError(code: "CERTIFICATE_ERROR", message: "failed to extract certificate", details: nil)
let ErrPrivateKeyExtraction = FlutterError(code: "PRIVATE_KEY_ERROR", message: "failed to extract private key", details: nil)
let ErrPrivateKeyConvert = FlutterError(code: "PRIVATE_KEY_ERROR", message: "failed to convert private key", details: nil)

public class SwiftDSignaturePlugin: NSObject, FlutterPlugin {
    
    public static func register(with registrar: FlutterPluginRegistrar) {
      let channel = FlutterMethodChannel(name: "flutter_dsig", binaryMessenger: registrar.messenger())
      let instance = SwiftDSignaturePlugin()
      registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
      do {
          try executeFunc(call: call, result: result)
      } catch {
          result(FlutterError(code: "UNKNOWN_ERROR", message: "", details: error.localizedDescription ))
      }
    }

    private func executeFunc(call: FlutterMethodCall, result:FlutterResult) throws {
      let dic = call.arguments as? [String: Any]
      switch call.method {
      case "getPlatformVersion":
          result("iOS " + UIDevice.current.systemVersion)
      case "getCertificateFromP12":
          let p12 = dic?["p12"] as! FlutterStandardTypedData
          try self.getCertificateFromP12(p12.data as NSData, dic?["password"] as! NSString, result)
      case "getPrivateKeyFromP12":
          let p12 = dic?["p12"] as! FlutterStandardTypedData
          try self.getPrivateKeyFromP12(p12.data as NSData, dic?["password"] as! NSString, result)
      case "getNodesFromP12":
          print("Method not found")
          result(FlutterMethodNotImplemented)
//          let p12 = dic?["p12"] as! FlutterStandardTypedData
//          try self.getNodesFromP12(p12.data as NSData, dic?["password"] as! NSString, result)
      case "signDataWithP12":
          let data = dic?["data"] as! FlutterStandardTypedData
          let p12 = dic?["p12"] as! FlutterStandardTypedData
          try self.signWithP12(p12.data as NSData, dic?["password"] as! NSString, data.data as NSData, result)
      default:
          print("Method not found")
          result(FlutterMethodNotImplemented)
      }
    }

    private func getCertificateFromP12(_ p12: NSData, _ password: NSString, _ result: FlutterResult) throws {
      let certificateResponse = self.getCertificate(p12, password)
      guard certificateResponse.error == errSecSuccess else {
          let secError = certificateResponse.error
          if secError == errSecAuthFailed {
              result(ErrBadPassword)
          } else {
              result(FlutterError(code: "FATAL_ERROR", message: "Failed to extract certificate: \(secError)", details: nil))
          }
          return
      }
      
      guard let certificate = certificateResponse.certificate else {
          result(ErrCertificateExtraction)
          return
      }
      
      let data = SecCertificateCopyData(certificate) as Data
    //      let string = (data as Data).base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
      result(data)
    }

    private func getPrivateKeyFromP12(_ p12: NSData, _ password: NSString, _ result: FlutterResult) throws {
        let privateKeyResponse = self.getPrivateKey(p12, password)
        guard privateKeyResponse.error == errSecSuccess else {
            let secError = privateKeyResponse.error
            if secError == errSecAuthFailed {
              result(ErrBadPassword)
            } else {
              result(FlutterError(code: "FATAL_ERROR", message: "Failed to extract private key: \(secError)", details: nil))
            }
            return
        }

        guard let privateKey = privateKeyResponse.privateKey else {
            result(ErrPrivateKeyExtraction)
            return
        }

        var error: Unmanaged<CFError>?
        guard let pkBytes = SecKeyCopyExternalRepresentation(privateKey, &error) as Data? else {
            result(ErrPrivateKeyConvert)
            return
        }
        result(pkBytes)
    }
    
    private func getNodesFromP12(_ p12: NSData, _ password: NSString, _ result: FlutterResult) throws {
        let nodesResponse = self.getNodes(p12, password)
        guard nodesResponse.error == errSecSuccess else {
            let secError = nodesResponse.error
            if secError == errSecAuthFailed {
              result(ErrBadPassword)
            } else {
              result(FlutterError(code: "FATAL_ERROR", message: "Failed to extract nodes: \(secError)", details: nil))
            }
            return
        }
        
        guard let certificate = nodesResponse.cert else {
            result(ErrCertificateExtraction)
            return
        }
        let certData = SecCertificateCopyData(certificate) as Data
        
        guard let privateKey = nodesResponse.pk else {
            result(ErrPrivateKeyExtraction)
            return
        }
        var error: Unmanaged<CFError>?
        guard let pkData = SecKeyCopyExternalRepresentation(privateKey, &error) as Data? else {
            result(ErrPrivateKeyConvert)
            return
        }
        
        result([certData, pkData])
    }

    private func signWithP12(_ p12: NSData, _ password: NSString, _ data: NSData, _ result: FlutterResult) throws {
      let privateKeyResponse = self.getPrivateKey(p12, password)
      guard privateKeyResponse.error == errSecSuccess else {
          let secError = privateKeyResponse.error
          if secError == errSecAuthFailed {
              result(ErrBadPassword)
          } else {
              result(FlutterError(code: "FATAL_ERROR", message: "No ha sido posible realizar la extraccion del certificado \(secError)", details: nil))
          }
          return
      }

      guard let privateKey = privateKeyResponse.privateKey else {
          result(FlutterError(code: "PRIVATE_KEY_ERROR", message: "No ha sido posible realizar la extraccion de la clave privada", details: nil))
          return
      }
      let resultSign : RSASigningResult = self.signWithPrivateKey(data: data as Data, privateKey: privateKey)
      guard resultSign.error == nil else {
          result(FlutterError(code: "ERROR_SIGN", message: "No ha sido posible firmar los datos con la clave privada \(resultSign.error!.description)", details: nil))
          return
      }
      result(resultSign.signedData)
    }

    private typealias RSASigningResult = (signedData: Data?, error: NSError?)
    private func signWithPrivateKey(data plainData: Data, privateKey: SecKey!) -> RSASigningResult {
      // Then sign it
      let dataToSign = [UInt8](plainData)
      var signatureLen = SecKeyGetBlockSize(privateKey)
      var signature = [UInt8](repeating: 0, count: SecKeyGetBlockSize(privateKey))

      let err: OSStatus = SecKeyRawSign(privateKey,
                                        SecPadding.PKCS1,
                                        dataToSign,
                                        plainData.count,
                                        &signature, &signatureLen)

      if err == errSecSuccess {
          return (signedData: Data(_: signature), error: nil)
      }

      return (signedData: nil, error: NSError(domain: NSOSStatusErrorDomain, code: Int(err), userInfo: nil))
    }

    private typealias IdentityResult = (identity: SecIdentity?, error: OSStatus)
    private func getIdentity(_ data: NSData, _ password: NSString) -> IdentityResult{
      let importPasswordOption:NSDictionary = [ kSecImportExportPassphrase : password]
      var items : CFArray?
      let secError : OSStatus = SecPKCS12Import(data,importPasswordOption, &items)
      guard secError == errSecSuccess else {
          return IdentityResult( identity: nil, error: secError )
      }
      let identityDictionaries = items as! [[String: Any]]
      let identity = identityDictionaries[0][kSecImportItemIdentity as String] as! SecIdentity
      return IdentityResult(identity: identity, error: errSecSuccess )
    }

    private typealias CertificateResult = (certificate: SecCertificate?, error: OSStatus)
    private func getCertificate(_ data: NSData, _ password: NSString) -> CertificateResult {
      let identityResult = getIdentity(data, password)
      guard identityResult.error == errSecSuccess else {
          return CertificateResult(certificate: nil, error: identityResult.error)
      }

      guard let identity = identityResult.identity else {
          return CertificateResult( certificate: nil, error: errSecBadReq )
      }

      var optCertificateRef : SecCertificate?
      let secError = SecIdentityCopyCertificate(identity, &optCertificateRef)
      guard secError == errSecSuccess else {
          return CertificateResult( certificate : nil, error: secError )
      }

      guard let certificate = optCertificateRef else {
          return CertificateResult( certificate : nil,  error: errSecBadReq )
      }

      return CertificateResult( certificate : certificate, error: errSecSuccess)
    }

    private typealias PrivateKeyResult = (privateKey: SecKey?, error: OSStatus)
    private func getPrivateKey(_ data:NSData, _ password: NSString ) -> PrivateKeyResult {
      let identityResult = getIdentity(data, password)
      guard identityResult.error == errSecSuccess else {
          return PrivateKeyResult(privateKey: nil, error: identityResult.error)
      }

      guard let identity = identityResult.identity else {
          return PrivateKeyResult( privateKey: nil, error: errSecBadReq )
      }

      var optPrivateKey: SecKey?
      let secError = SecIdentityCopyPrivateKey(identity, &optPrivateKey)
      guard secError == errSecSuccess else {
          return PrivateKeyResult(privateKey: nil, error: secError )
      }
      
      guard let privateKey = optPrivateKey else {
          return PrivateKeyResult(privateKey: nil, error: errSecBadReq )
      }
      
      return PrivateKeyResult( privateKey: privateKey, error: errSecSuccess )
    }
    
    private typealias NodesResult = (cert: SecCertificate?, pk: SecKey?, error: OSStatus)
    private func getNodes(_ data:NSData, _ password: NSString ) -> NodesResult {
        let identityResult = getIdentity(data, password)
        guard identityResult.error == errSecSuccess else {
            return NodesResult(cert: nil, pk: nil, error: identityResult.error)
        }
        
        guard let identity = identityResult.identity else {
            return NodesResult(cert: nil, pk: nil, error: errSecBadReq)
        }
        
        // certificate
        var optCertificateRef : SecCertificate?
        let secError = SecIdentityCopyCertificate(identity, &optCertificateRef)
        guard secError == errSecSuccess else {
            return NodesResult(cert: nil, pk: nil, error: secError)
        }
        guard let certificate = optCertificateRef else {
            return NodesResult(cert: nil, pk: nil, error: errSecBadReq)
        }
        
        // private key
        var optPrivateKey: SecKey?
        let secError2 = SecIdentityCopyPrivateKey(identity, &optPrivateKey)
        guard secError2 == errSecSuccess else {
            return NodesResult(cert: nil, pk: nil, error: secError2)
        }
        guard let privateKey = optPrivateKey else {
            return NodesResult(cert: nil, pk: nil, error: errSecBadReq)
        }
        
        return NodesResult(cert: certificate, pk: privateKey, error: errSecBadReq)
        
    }

}
