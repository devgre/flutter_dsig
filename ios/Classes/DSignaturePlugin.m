#import "DSignaturePlugin.h"
#if __has_include(<flutter_dsig/flutter_dsig-Swift.h>)
#import <flutter_dsig/flutter_dsig-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_dsig-Swift.h"
#endif

@implementation DSignaturePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftDSignaturePlugin registerWithRegistrar:registrar];
}
@end
